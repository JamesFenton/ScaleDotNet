﻿angular.module('ScaleApp', [])
  .controller('WorkersController', function ($http) {
      var server = this;
      server.workers = [];
      server.workUnitSummary = {};
      server.stats = {};

        var getWorkers = function () {
          $http.get("/workers")
              .then(function (response) {
                  server.workers = response.data;
              });
      };
      getWorkers();

      var getWorkUnitSummary = function () {
          $http.get("/workUnits")
              .then(function (response) {
                  server.workUnitSummary = response.data;
              });
      };
      getWorkUnitSummary();

      server.addWork = function () {
          $http.get("/addWork")
              .then(function (response) {
                  server.workUnitSummary = response.data;
              });
      };

      var getStats = function () {
          $http.get("/stats")
              .then(function (response) {
                  server.stats = response.data;
              });
      };
        getStats();

      setInterval(getWorkers, 5000);
      setInterval(getWorkUnitSummary, 5000);
      setInterval(getStats, 5000);
});