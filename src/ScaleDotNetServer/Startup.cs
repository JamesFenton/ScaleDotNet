﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNet.Builder;
using Microsoft.AspNet.Hosting;
using Microsoft.AspNet.Http;
using Microsoft.Extensions.DependencyInjection;
using System.IO;
using Amazon;
using Amazon.DynamoDBv2;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2.DocumentModel;
using Amazon.Runtime;
using Newtonsoft.Json;
using ScaleDotNet.Core;
using Amazon.SQS;
using Microsoft.Extensions.Configuration;

namespace ScaleDotNetServer
{
    public class Startup
    {
        IConfiguration Config;

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit http://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            Config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

            var random = new Random();
            services.AddInstance(random);

            var client = new AmazonDynamoDBClient(new EnvironmentVariablesAWSCredentials(), RegionEndpoint.EUWest1);
            var context = new DynamoDBContext(client);
            services.AddInstance(context);
            
            var queueClient = new AmazonSQSClient(new EnvironmentVariablesAWSCredentials(), RegionEndpoint.EUWest1);
            services.AddInstance(queueClient);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app)
        {
            app.UseIISPlatformHandler();

            app.UseDefaultFiles();
            app.UseStaticFiles();
            
            var random = app.ApplicationServices.GetService<Random>();
            var db = app.ApplicationServices.GetService<DynamoDBContext>();
            var queueClient = app.ApplicationServices.GetService<AmazonSQSClient>();
            var queueUrl = queueClient.CreateQueue(Config["WorkUnitQueueName"]).QueueUrl;

            // state
            var workers = new List<ScaleWorker>();
            long timeToGetWorkers = 0;
            var workUnitSummary = new WorkUnitSummary();
            long timeToGetWorkUnitSummary = 0;

            Task.Run(() =>
                     {
                         var sw = new Stopwatch();
                         while (true)
                         {
                             try
                             {
                                 sw.Restart();
                                 workers = db.Scan<ScaleWorker>().ToList();
                                 timeToGetWorkers = sw.ElapsedMilliseconds;
                                 sw.Restart();
                                 workUnitSummary = GetWorkUnitSummary(db, queueClient, queueUrl);
                                 timeToGetWorkUnitSummary = sw.ElapsedMilliseconds;
                             }
                             catch (Exception e)
                             {
                                 Console.WriteLine(e);
                             }
                             Task.Delay(5000).Wait();
                         }
                     });

            app.Run(context =>
                          {
                              if (context.Request.Path.StartsWithSegments("/workers"))
                              {
                                  return context.Response.WriteAsync(JsonConvert.SerializeObject(workers));
                              }
                              else if (context.Request.Path.StartsWithSegments("/workUnits"))
                              {
                                  return context.Response.WriteAsync(JsonConvert.SerializeObject(workUnitSummary));
                              }
                              else if (context.Request.Path.StartsWithSegments("/addWork"))
                              {
                                  for (int i = 0; i < 10; i++)
                                  {
                                      try
                                      {
                                          var workUnit = GenerateRandomWorkUnit(random);
                                          db.Save(workUnit);
                                          queueClient.SendMessage(queueUrl, workUnit.Id.ToString());
                                      }
                                      catch (Exception e)
                                      {
                                          Console.WriteLine(e);
                                      }
                                  }
                                  return context.Response.WriteAsync("Added 10 work units");
                              }
                              else if (context.Request.Path.StartsWithSegments("/stats"))
                              {
                                  return context.Response.WriteAsync($"{{\"timeToGetWorkers\": \"{timeToGetWorkers}\", \"timeToGetWorkUnitSummary\": \"{timeToGetWorkUnitSummary}\" }}");
                              }
                              else
                              {
                                  return context.Response.WriteAsync("");
                              }
                          });
        }

        private static WorkUnitSummary GetWorkUnitSummary(DynamoDBContext db, AmazonSQSClient queueClient, string queueUrl)
        {
            var workUnitCount = db.Scan<WorkUnit>().Count();
            var workUnitsIncomplete = db.Scan<WorkUnit>(new ScanCondition("Result", ScanOperator.Equal, 0)).Count();
            var workUnitsInProgress = queueClient.GetQueueAttributes(queueUrl, new List<string> { "All" }).ApproximateNumberOfMessagesNotVisible;
            var workUnitsCompleted = db.Scan<WorkUnit>(new ScanCondition("Result", ScanOperator.NotEqual, 0)).Count();
            var workUnitQueueLength = queueClient.GetQueueAttributes(queueUrl, new List<string> { "All" }).ApproximateNumberOfMessages;
            var averageWorkUnitTimeToComplete = workUnitCount == 0 ? 0 : db.Scan<WorkUnit>().Where(wu => wu.TimeToComplete != 0).Average(wu => wu.TimeToComplete);

            return new WorkUnitSummary
                   {
                       WorkUnitCount = workUnitCount,
                       WorkUnitsIncomplete = workUnitsIncomplete,
                       WorkUnitsInProgress = workUnitsInProgress,
                       WorkUnitsCompleted = workUnitsCompleted,
                       WorkUnitQueueLength = workUnitQueueLength,
                       AverageWorkUnitTimeToComplete = (int)averageWorkUnitTimeToComplete
                   };
        }

        private static WorkUnit GenerateRandomWorkUnit(Random r)
        {
            var work = r.Next(Int32.MaxValue/2);
            var workUnit = new WorkUnit { Id = work.GetHashCode(), Work = work, SubmittedAt = DateTime.Now.ToString("o") };
            return workUnit;
        }

        // Entry point for the application.
        public static void Main(string[] args) => WebApplication.Run<Startup>(args);
    }
}
