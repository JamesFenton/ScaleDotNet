﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Amazon.DynamoDBv2.DataModel;
using Newtonsoft.Json;

namespace ScaleDotNet.Core
{
    [DynamoDBTable("ScaleDotNet.Workers")]
    public class ScaleWorker
    {
        [DynamoDBHashKey]
        public string Name { get; set; }
        
        public string LastCheckIn { get; set; }

        public string State { get; set; }

        public void SetState(State state)
        {
            State = state.ToString();
        }

        /// <summary>
        /// Returns a string that represents the current object.
        /// </summary>
        /// <returns>
        /// A string that represents the current object.
        /// </returns>
        public override string ToString()
        {
            return JsonConvert.SerializeObject(this, Formatting.Indented);
        }
    }

    public enum State
    {
        Idle,
        InProgress
    }
}
