﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScaleDotNet.Core
{
    public class WorkUnitSummary
    {
        public int WorkUnitCount { get; set; }

        public int WorkUnitsIncomplete { get; set; }

        public int WorkUnitsInProgress { get; set; }

        public int WorkUnitsCompleted { get; set; }

        public int WorkUnitQueueLength { get; set; }

        public int AverageWorkUnitTimeToComplete { get; set; }
    }
}
