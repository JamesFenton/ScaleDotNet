﻿using Amazon.DynamoDBv2.DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ScaleDotNet.Core
{
    [DynamoDBTable("ScaleDotNet.WorkUnits")]
    public class WorkUnit
    {
        [DynamoDBHashKey]
        public int Id { get; set; }

        public int Work { get; set; }

        public int Result { get; set; }

        public string SubmittedAt { get; set; }

        public string CompletedAt { get; set; }

        public long TimeToComplete { get; set; }

        public string Worker { get; set; }
    }
}
