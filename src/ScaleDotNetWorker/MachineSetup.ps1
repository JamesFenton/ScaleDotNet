﻿# clone
git clone https://gitlab.com/JamesFenton/ScaleDotNet.git C:\apps\ScaleDotNet
# restore & run
$dnxPath = "C:\Users\james\.dnx\runtimes\dnx-clr-win-x86.1.0.0-rc1-update1\bin"
& "$dnxPath\dnu" restore "C:\apps\ScaleDotNet"
& "$dnxPath\dnx" -p "C:\apps\ScaleDotNet\src\ScaleDotNetWorker" run | Out-File "C:\users\james\desktop\ScaleDotNet.log"