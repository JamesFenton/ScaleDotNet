﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Amazon;
using Amazon.SQS;
using ScaleDotNet.Core;
using Amazon.Runtime;
using Amazon.DynamoDBv2.DataModel;
using Amazon.DynamoDBv2;
using System.Net.Http;
using Microsoft.Extensions.Configuration;

namespace ScaleDotNetWorker
{
    public class Program
    {
        static IConfiguration Config;

        private static string Name = Environment.MachineName;

        public static void Main(string[] args)
        {
            Config = new ConfigurationBuilder().AddJsonFile("appsettings.json").Build();

            var dbClient = new AmazonDynamoDBClient(new EnvironmentVariablesAWSCredentials(), RegionEndpoint.EUWest1);
            var db = new DynamoDBContext(dbClient);
            var queueClient = new AmazonSQSClient(new EnvironmentVariablesAWSCredentials(), RegionEndpoint.EUWest1);
            var queueUrl = queueClient.CreateQueue(Config["WorkUnitQueueName"]).QueueUrl;

            var state = State.Idle;
            // check in
            Task.Run(() =>
            {
                while (true)
                {
                    CheckIn(db, state);
                    Task.Delay(new TimeSpan(0, 0, 1)).Wait();
                }
            });

            while (true)
            {
                try
                {
                    string receiptHandle = null;
                    var workUnit = TryGetWork(db, queueClient, queueUrl, ref receiptHandle);
                    if (workUnit != null)
                    {
                        state = State.InProgress;
                        Console.WriteLine($"Working on work: {workUnit.Work}");
                        WorkUnit result = Work(workUnit);
                        SaveResult(workUnit, receiptHandle, db, queueClient, queueUrl);
                        Console.WriteLine($"Saved result {result.Result}");
                        state = State.Idle;
                    }
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
                Task.Delay(new TimeSpan(0, 0, 1)).Wait();
            }
        }

        private static void CheckIn(DynamoDBContext db, State state)
        {
            var worker = new ScaleWorker { Name = Name, LastCheckIn = DateTime.Now.ToString("o")};
            worker.SetState(state);
            db.Save(worker);
        }

        private static WorkUnit TryGetWork(DynamoDBContext db, AmazonSQSClient queueClient, string queueUrl, ref string receiptHandle)
        {
            var response = queueClient.ReceiveMessage(queueUrl);
            var message = response.Messages.FirstOrDefault();
            if (message == null)
                return null;

            receiptHandle = message.ReceiptHandle;
            var workUnit = db.Load<WorkUnit>(Int32.Parse(message.Body));
            return workUnit;
        }

        private static WorkUnit Work(WorkUnit workUnit)
        {
            var sw = new Stopwatch();
            sw.Start();
            Task.Delay(new TimeSpan(0, 0, 15)).Wait(); // pretend work
            sw.Stop();
            workUnit.Result = workUnit.Work * 2;
            workUnit.CompletedAt = DateTime.Now.ToString("o");
            workUnit.Worker = Name;
            workUnit.TimeToComplete = sw.ElapsedMilliseconds;
            return workUnit;
        }

        private static void SaveResult(WorkUnit workUnit, string receiptHandle, DynamoDBContext db, AmazonSQSClient queueClient, string queueUrl)
        {
            db.Save(workUnit);
            var deleteResponse = queueClient.DeleteMessage(queueUrl, receiptHandle);
        }
    }
}
